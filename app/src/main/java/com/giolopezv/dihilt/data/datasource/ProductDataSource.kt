package com.giolopezv.dihilt.data.datasource

import com.giolopezv.dihilt.domain.model.Product

import java.util.logging.Logger
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProductDataSource @Inject constructor() {

    private val logger = Logger.getLogger(ProductDataSource::class.simpleName)

    suspend fun getProducts(): List<Product> = listOf(
        Product(
            "1",
            "Lavadora",
            450000.00,
            "https://falabella.scene7.com/is/image/FalabellaCO/3399669_1?wid=800&hei=800&qlt=70"
        ),
        Product(
            "2",
            "Televisor",
            670000.00,
            "https://falabella.scene7.com/is/image/FalabellaCO/5688505_1?wid=800&hei=800&qlt=70"
        ),
        Product(
            "3",
            "Nevera",
            700000.00,
            "https://falabella.scene7.com/is/image/FalabellaCO/4222473_1?wid=800&hei=800&qlt=70"
        ),
        Product(
            "4",
            "Estufa",
            770000.00,
            "https://falabella.scene7.com/is/image/FalabellaCO/1902137_1?wid=800&hei=800&qlt=70"
        ),
        Product(
            "5",
            "Xbox - One",
            680000.00,
            "https://falabella.scene7.com/is/image/Falabella/6050347_1?wid=800&hei=800&qlt=70"
        ),
        Product(
            "6",
            "Play station 4",
            830000.00,
            "https://falabella.scene7.com/is/image/FalabellaCO/3896177_1?wid=800&hei=800&qlt=70"
        ),
        Product(
            "7",
            "Moto One Fusion +",
            460000.00,
            "https://falabella.scene7.com/is/image/FalabellaCO/8206889_8?wid=800&hei=800&qlt=70"
        ),
        Product(
            "8",
            "Camioneta CR",
            160000.00,
            "https://falabella.scene7.com/is/image/FalabellaCO/881313507_1?wid=800&hei=800&qlt=70"
        )
    )

}
