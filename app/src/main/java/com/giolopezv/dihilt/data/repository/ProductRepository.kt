package com.giolopezv.dihilt.data.repository

import com.giolopezv.dihilt.domain.model.Product

interface ProductRepository {

    suspend fun getProducts(): List<Product>
}
