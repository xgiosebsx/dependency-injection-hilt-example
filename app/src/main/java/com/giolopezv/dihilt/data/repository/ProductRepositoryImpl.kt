package com.giolopezv.dihilt.data.repository

import com.giolopezv.dihilt.data.datasource.ProductDataSource
import com.giolopezv.dihilt.domain.model.Product

import java.util.logging.Logger
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProductRepositoryImpl @Inject constructor(private val productDataSource: ProductDataSource) : ProductRepository {

    private val logger = Logger.getLogger(ProductRepositoryImpl::class.simpleName)

    override suspend fun getProducts(): List<Product> = productDataSource.getProducts()

}
