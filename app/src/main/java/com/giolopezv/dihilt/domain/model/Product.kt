package com.giolopezv.dihilt.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Product(
    val id: String,
    val title: String,
    val price: Double,
    val image: String
):Parcelable
