package com.giolopezv.dihilt.domain.usescase
import com.giolopezv.dihilt.data.repository.ProductRepository
import com.giolopezv.dihilt.domain.model.Product
import javax.inject.Inject
import javax.inject.Singleton

class GetProductsByQueryUseCase @Inject constructor(private val productRepository: ProductRepository) {
    suspend operator fun invoke(): List<Product> = productRepository.getProducts()
}
