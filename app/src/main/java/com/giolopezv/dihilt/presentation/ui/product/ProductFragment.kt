package com.giolopezv.dihilt.presentation.ui.product

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.giolopezv.dihilt.databinding.FragmentProductBinding
import com.giolopezv.dihilt.domain.model.Product
import com.giolopezv.dihilt.presentation.ui.product.adapter.ProductAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductFragment : Fragment() {

    private val adapter: ProductAdapter = ProductAdapter(onItemClick = this::goToDetail)

    private val viewModel: ProductViewModel by viewModels()
    private lateinit var binding: FragmentProductBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProductBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        initViewStateObserver()
        setupRecycler()
        loadProducts()
    }

    private fun setViewState(viewState: ProductViewState) {
        when (viewState) {
            is ShowProducts -> showSuccessSearchProducts(viewState.data)
        }
    }

    private fun showSuccessSearchProducts(data: List<Product>) {
        setDataInList(data)
    }

    private fun setDataInList(data: List<Product>) {
        adapter.submitList(data)
    }

    private fun setupRecycler() {
        binding.rvProductList.adapter = adapter
    }

    private fun initViewStateObserver() {
        viewModel.viewState.observe(viewLifecycleOwner, Observer(::setViewState))
    }

    private fun loadProducts() {
        viewModel.searchProducts()
    }


    private fun goToDetail(product: Product) {
        val action = ProductFragmentDirections.actionProductFragmentToProductDetailFragment(product)
        findNavController().navigate(action)
    }
}
