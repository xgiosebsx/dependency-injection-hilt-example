package com.giolopezv.dihilt.presentation.ui.product

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.giolopezv.dihilt.presentation.di.IoDispatcher
import com.giolopezv.dihilt.domain.usescase.GetProductsByQueryUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductViewModel @Inject constructor(
    private val getProductsByQueryUseCaseByConstructor: GetProductsByQueryUseCase,
    @IoDispatcher private val dispatcher: CoroutineDispatcher
) : ViewModel() {

    var viewState: MutableLiveData<ProductViewState> = MutableLiveData()

    @Inject
    lateinit var getProductsByQueryUseCaseByProperty: GetProductsByQueryUseCase

    fun searchProducts() {
        viewModelScope.launch(dispatcher) {
            val response = getProductsByQueryUseCaseByConstructor()
            viewState.postValue(ShowProducts(response))
        }
    }

}
