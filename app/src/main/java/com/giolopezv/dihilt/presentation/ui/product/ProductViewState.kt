package com.giolopezv.dihilt.presentation.ui.product

import com.giolopezv.dihilt.domain.model.Product

sealed class ProductViewState
data class ShowProducts(val data: List<Product>): ProductViewState()

