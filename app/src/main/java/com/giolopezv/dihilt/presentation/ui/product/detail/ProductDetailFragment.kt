package com.giolopezv.dihilt.presentation.ui.product.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.giolopezv.dihilt.databinding.FragmentProductDetailBinding
import com.giolopezv.dihilt.domain.model.Product
import com.giolopezv.dihilt.presentation.util.loadUrl
import com.giolopezv.dihilt.presentation.util.toCurrency
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductDetailFragment : Fragment() {

    private val args: ProductDetailFragmentArgs by navArgs()

    private lateinit var binding: FragmentProductDetailBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProductDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        val product = args.product
        setDetailInfo(product)
    }

    private fun setDetailInfo(product: Product) {
        binding.tvProductName.text = product.title
        binding.tvProductPrice.text = product.price.toCurrency()
        binding.ivProduct.loadUrl(product.image)
    }
}
