package com.giolopezv.dihilt.presentation.ui.product

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.giolopezv.dihilt.domain.model.Product
import com.giolopezv.dihilt.domain.usescase.GetProductsByQueryUseCase
import com.giolopezv.dihilt.presentation.util.getOrAwaitValue
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class ProductViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private var testCoroutineDispatcher = TestCoroutineDispatcher()

    @MockK
    private lateinit var getProductsByQueryUseCase: GetProductsByQueryUseCase

    private lateinit var viewModel: ProductViewModel

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        viewModel = ProductViewModel(getProductsByQueryUseCase, testCoroutineDispatcher)
    }

    @Test
    fun `searchProducts should call getProductsByQueryUseCase()`() =
        runBlockingTest {
            // Given
            val data = listOf(mockk<Product>())

            coEvery { getProductsByQueryUseCase() } returns data

            // When
            viewModel.searchProducts()

            // Then
            coVerify { getProductsByQueryUseCase() }
        }

    @Test
    fun `searchProducts should emit SearchProductsSuccess with data status through a liveData when getProductsByQueryUseCase returns success`() =
        runBlockingTest {
            // Given
            val data = listOf(mockk<Product>())
            coEvery { getProductsByQueryUseCase() } returns data

            // When
            viewModel.searchProducts()
            val result = viewModel.viewState.getOrAwaitValue()

            // Then
            assertEquals(ShowProducts(data), result)
        }
}
